# Deployment for Monadical take-home project

This deployment uses Ansible and imports two helper roles:

- **ansible-nginx-revproxy** for installing Nginx and configuring letsencrypt certs
- **ansible-role-docker** for deploying docker to our host server

We deploy Nginx to the host instead of declaring it in the `docker-compose.yml` file for the main project. This allows Nginx to proxy multiple different apps that may coexist on the server, and maintains separation between app and webserver in the case we wanted to deploy the app using traefik, a cloud-based load balancer, or in a micro-services environment instead.

## Requirements

- `ansible` version 2.9
- A fresh Debian-based VPS (Vultr in this example)
- Root access with SSH key generated and added to VPS via host panel
- Domain name A record created and pointed to server public IP

## Steps to deploy

Clone this repository

    git clone https://bitbucket.org/Grimm665/monadical-deployment.git
    cd monadical-deployment

Retrieve git submodules

    git submodule update --recursive --init

Make any modifications to the vars in the deploy playbook

    vim deploy.yml

Run the deploy playbook

    ansible-playbook deploy.yml -vv -u root -i '<SERVER_IP_ADDRESS>,'


## Things I would consider doing differently

### Nginx
The project specified nginx as the webserver, and in this case I decided to
deploy nginx on the host rather than include it in the Docker compose setup. A
more Docker-friendly solution would be to use Traefik with its auto-ssl as the
reverse proxy. Configuration would be much smaller and we would not need a
separate container just to manage SSL, as we would with most dockerized Nginx
solutions.

Specifying nginx would make more sense if the server was expected to host more
than one app, which is the assumption I made here. Deploying to the host would
make the most sense when taking this into consideration.

### Logging
The project specifies all app, webserver, and database logs be stored in the
data/logs folder. If deploying natively with packages, and without Docker, this
would make sense to keep logs organized, as described in the
[blogpost](https://docs.monadical.com/s/an-intro-to-the-opt-directory).  With
docker-compose, logs are collected using the `docker-compose logs` command, and
it makes more sense to ship these logs to a log service like Graylog or ELK
rather than capture them locally. My scripts located in `bin` do capture these
logs locally, but not in a robust way. Rebooting the server disconnects the log, for
example.

Additionally, since Nginx is deployed on the host and the assumption being it is
proxying more than just this app, as a sysadmin I would not expect Nginx
access/error logs to be stored within the app folder itself, I would expect
these to be in `/var/log` with the rest of the Nginx logs. If we deployed the
reverse proxy (traefik or Nginx) with docker-compose as part of the app, then I
would agree the logs for the webserver should be stored locally with the app.

### Firewall
UFW would not have been my first choice for a firewall, but it should be
adequate in this scenario. Debian does not ship with a fully working `firewalld`
package last time I checked, which would have been my preference since it
integrates very well with Ansible and Docker. It's all a bit moot though, since
the proper way to secure this server would probably be to use security groups in
the VPS hosting panel, or to put the whole app behind a load balancer or
Cloudflare Argo tunnel, as recommended in the Bonus section of the project
description.

### Forking the example code vs a no-touch deployment style
Since we are using Ansible as our deployment mechanism, the general assumption
would be that Ansible would handle all upgrades and changes to the app and the
server infrastructure. As such, we would not need to fork the Monadical example
devops project from GitHub and add our Dockerfiles and apply our config changes
there. Instead, we would include the files and changes as Ansible templates and
steps as part of the deployment. This would free us from the burden of
maintaining our fork, instead we would just apply whatever changes occur
upstream and add our deployment changes on top using Ansible.

However, when looking at the project description and remembering that Monadical
is a software consulting company, it is likely that after the deployment is
done, the customer would maintain the server and would not expect to use Ansible
to do so. With this in mind, I chose not to use the no-touch deployment style,
and instead fork the repo and apply my changes there. Then, using Ansible, we
deploy the forked repo that contains all the necessary parts to run the app
without relying on Ansible to maintain the app configuration.
